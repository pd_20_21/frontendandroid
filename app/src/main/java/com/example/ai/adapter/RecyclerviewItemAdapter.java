package com.example.ai.adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ai.R;
import com.example.ai.fragment.RouteList;
import com.example.ai.model.Result;
import com.example.ai.model.Route;
import com.example.ai.retrofit.APICall;
import com.example.ai.retrofit.RetrofitClient;
import com.mapbox.geojson.MultiPolygon;

import java.text.DecimalFormat;
import java.util.List;

import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RecyclerviewItemAdapter extends RecyclerView.Adapter<RecyclerviewItemAdapter.ViewHolder> {
    private List<Result> itemsList;
    private RecyclerViewClickListener listener;
    View view;

    public RecyclerviewItemAdapter(List<Result> mItemList, RecyclerViewClickListener listener){
        this.itemsList = mItemList;
        this.listener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_row,parent,false);
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        String activity, difficulty, distance;
        final Result item = itemsList.get(position);
        activity = "Actividade: " + transformActivityIdToType(item.getActivityId());
        difficulty = "Dificuldade: " + transformDifficultIdToType(item.getDifficulty());
        distance = "Distancia: " + getDistanceWithOneDecimal(item.getDistance()) + " km";
        holder.name.setText(item.getRouteName());
        holder.activity.setText(activity);
        holder.difficult.setText(difficulty);
        holder.distance.setText(distance);
    }

    public String getDistanceWithOneDecimal(String distance) {
        double dist = Double.parseDouble(distance);
        DecimalFormat df = new DecimalFormat("#.#");
        distance = df.format(dist);
        return distance;
    }

    public String transformActivityIdToType(int activityId) {
        if (activityId == 1) {
            return "Caminhada";
        }
        if (activityId == 2) {
            return "Corrida";
        }
        if (activityId == 3) {
            return "Bicicleta";
        }
        return null;
    }

    public String transformDifficultIdToType(int difficultyId) {
        if (difficultyId == 1) {
            return "Muito Fácil";
        }
        if (difficultyId == 2) {
            return "Fácil";
        }
        if (difficultyId == 3) {
            return "Média";
        }
        if (difficultyId == 4) {
            return "Difícil";
        }
        if (difficultyId == 5) {
            return "Muito Difícil";
        }
        return null;
    }

    @Override
    public int getItemCount() {
        return itemsList.size();
    }

    public void setRouteList(List<Result> routeList) {
        itemsList = routeList;
    }


    class ViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        public TextView name,activity, difficult, distance;
        private LinearLayout itemLayout;

        public ViewHolder(View itemView) {
            super(itemView);
            name = itemView.findViewById(R.id.routeName);
            activity = itemView.findViewById(R.id.activityType);
            difficult = itemView.findViewById(R.id.difficultyType);
            distance = itemView.findViewById(R.id.distanceType);
            itemLayout =  itemView.findViewById(R.id.itemLayout);
            view.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            listener.onClick(view, getAdapterPosition());
            //routeListRequest();
        }
    }

    public interface RecyclerViewClickListener {
        void onClick(View v, int position);
    }
}
