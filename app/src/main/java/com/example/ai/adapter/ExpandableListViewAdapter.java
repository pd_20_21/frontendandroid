package com.example.ai.adapter;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.graphics.Typeface;
import android.text.Editable;
import android.text.InputFilter;
import android.text.InputType;
import android.text.Spanned;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseExpandableListAdapter;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.TextView;

import com.example.ai.R;
import com.example.ai.fragment.FilterHeader;
import com.example.ai.fragment.RouteList;
import com.google.android.material.textfield.TextInputEditText;
import com.google.android.material.textfield.TextInputLayout;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

public class ExpandableListViewAdapter extends BaseExpandableListAdapter {
    int childEditing;
    int groupEditing;
    private Activity _context;
    private ArrayList<FilterHeader> _listDataHeader;
    private HashMap<String, ArrayList<String>> _listDataChild;

    public HashMap<Integer, ArrayList<String>> listOfStatusFilters = new HashMap<>();
    public HashMap<String, String> childDistanceState = new HashMap<>();
    public HashMap<String, String> childDurationState = new HashMap<>();
    RouteList parentFragment;

    HashMap<Integer, ArrayList<Integer>> childCheckboxState = new HashMap<>();

    public ExpandableListViewAdapter(Activity _context, ArrayList<FilterHeader> _listDataHeader, HashMap<String, ArrayList<String>> _listDataChild, RouteList parentFragment) {
        this._context = _context;
        this._listDataHeader = _listDataHeader;
        this._listDataChild = _listDataChild;
        this.parentFragment = parentFragment;
        childCheckboxState.put(0, new ArrayList<Integer>());
        childCheckboxState.put(1, new ArrayList<Integer>());
        listOfStatusFilters.put(0, new ArrayList<String>());
        listOfStatusFilters.put(1, new ArrayList<String>());
        listOfStatusFilters.put(2, new ArrayList<String>());
        listOfStatusFilters.put(3, new ArrayList<String>());
    }

    @Override
    public int getGroupCount() {
        return this._listDataHeader.size();
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition).getTitle())
                .size();
    }

    @Override
    public FilterHeader getGroup(int groupPosition) {
        return this._listDataHeader.get(groupPosition);
    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return this._listDataChild.get(this._listDataHeader.get(groupPosition).getTitle())
                .get(childPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        String headerTitle = (String) getGroup(groupPosition).getTitle();
        LayoutInflater inflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = Objects.requireNonNull(inflater).inflate(R.layout.filter_header_layout, null);

        TextView lblListHeader = convertView.findViewById(R.id.lblListHeader);
        lblListHeader.setTypeface(null, Typeface.BOLD);
        lblListHeader.setText(headerTitle);
        TextView groupStatus = convertView.findViewById(R.id.groupStatus);

        groupStatus.setText(_listDataHeader.get(groupPosition).getActiveFilter());


        return convertView;
    }

    @SuppressLint("InflateParams")
    @Override
    public View getChildView(final int groupPosition, final int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) this._context
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        convertView = Objects.requireNonNull(inflater).inflate(R.layout.filter_child_layout, null);
        final CheckBox filterCheckBox = convertView.findViewById(R.id.filterNameCheckBox);
        final EditText editText = convertView.findViewById(R.id.distance_field);
        editText.setInputType(InputType.TYPE_CLASS_NUMBER | InputType.TYPE_NUMBER_FLAG_DECIMAL);
        View view = parent.getChildAt(groupPosition);
        final String childText = (String) getChild(groupPosition, childPosition);
        final FilterHeader headerText = (FilterHeader) getGroup(groupPosition);

        if (groupPosition < 2) {

            final TextView filterName = convertView
                    .findViewById(R.id.textviewFilterName);
            filterName.setText(childText);

            editText.setVisibility(View.GONE);
            filterCheckBox.setVisibility(View.VISIBLE);


            try {
                if (childCheckboxState.get(groupPosition).size() > 0 && childCheckboxState.get(groupPosition).contains(childPosition)) {
                    filterCheckBox.setChecked(true);
                }
                else {
                    filterCheckBox.setChecked(false);
                }
                notifyDataSetChanged();

                filterCheckBox.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        if (filterCheckBox.isChecked()) {
                            childCheckboxState.get(groupPosition).add(childPosition);
                            listOfStatusFilters.get(groupPosition).add(_listDataChild.get(headerText.getTitle()).get(childPosition));
                            parentFragment.addFilter(headerText.getTitle(), _listDataChild.get(headerText.getTitle()).get(childPosition));
                        } else {
                            childCheckboxState.get(groupPosition).remove((Integer) childPosition);
                            listOfStatusFilters.get(groupPosition).remove(_listDataChild.get(headerText.getTitle()).get(childPosition));
                            parentFragment.removeFilter(headerText.getTitle(), _listDataChild.get(headerText.getTitle()).get(childPosition));
                        }
                        if (_listDataChild.get(headerText.getTitle()) != null)
                            headerText.setActiveFilter(getCheckedStatusCombinedString(groupPosition));
                        notifyDataSetChanged();

                    }
                });

            } catch (Exception e) {

            }
        }
        else {

            filterCheckBox.setVisibility(View.GONE);
            editText.setHint(childText);
            editText.setVisibility(View.VISIBLE);

            try {
                if (groupPosition == 2) {
                    if (childDistanceState.containsKey(childText) && childDistanceState.get(childText) != null) {
                        editText.setText(childDistanceState.get(childText));
                    }
                    else {
                        editText.setText(null);
                    }
                }
                else {
                    if (childDurationState.containsKey(childText) && childDurationState.get(childText) != null) {
                        editText.setText(childDurationState.get(childText));
                    }
                    else {
                        editText.setText(null);
                    }
                }
                if (childPosition == childEditing && groupEditing == groupPosition) {
                    editText.requestFocus();
                }

                editText.addTextChangedListener(new TextWatcher() {
                    @Override
                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

                    }

                    @Override
                    public void onTextChanged(CharSequence s, int start, int before, int count) {

                    }

                    @Override
                    public void afterTextChanged(Editable s) {
                        parentFragment.addFilter(childText, s.toString());
                        if (groupPosition == 2) {
                            childDistanceState.put(childText, s.toString());
                            headerText.setActiveFilter(getDistanceStatusCombinedString());
                        }
                        else {
                            childDurationState.put(childText, s.toString());
                            headerText.setActiveFilter(getDurationStatusCombinedString());
                        }
                        childEditing = childPosition;
                        groupEditing = groupPosition;
                        notifyDataSetChanged();
                    }
                });
            }
            catch (Exception e) {

            }
        }

        return convertView;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

    public String getDistanceStatusCombinedString() {
        String status = "Min: ";
        if (childDistanceState.get("Distancia Minima") == null || childDistanceState.get("Distancia Minima").equals("")){
            status += "0 , Max: ";
        }
        else {
            status += childDistanceState.get("Distancia Minima") + " , Max: ";
        }

        if (childDistanceState.get("Distancia Maxima") != null && !childDistanceState.get("Distancia Maxima").equals("")) {
            status += childDistanceState.get("Distancia Maxima");
        }
        return status;
    }

    public String getDurationStatusCombinedString() {
        String status = "Min: ";
        if (childDurationState.get("Duracao Minima") == null || childDurationState.get("Duracao Minima").equals("")){
            status += "0 , Max: ";
        }
        else {
            status += childDurationState.get("Duracao Minima") + " , Max: ";
        }

        if (childDurationState.get("Duracao Maxima") != null && !childDurationState.get("Duracao Maxima").equals("")) {
            status += childDurationState.get("Duracao Maxima");
        }
        return status;
    }


    public String getCheckedStatusCombinedString(int groupPosition) {
        String status = "";

        for (int i = 0; i < listOfStatusFilters.get(groupPosition).size(); i++) {
            status += listOfStatusFilters.get(groupPosition).get(i);
            if (i != listOfStatusFilters.get(groupPosition).size() - 1) {
                status += ", ";
            }
        }
        if (status == "") {
            return "Nenhum";
        }
        return status;
    }

    public void resetFilter() {
        this._listDataHeader = getDefaultHeader();
        //this._listDataChild = _listDataChild;
        cleanCheckboxes();
        cleanDistancesAndDurations();
        setHeaderTextToDefault();
        notifyDataSetChanged();
    }

    public ArrayList<FilterHeader> getDefaultHeader() {
        ArrayList<FilterHeader> _listDataHeader = new ArrayList<>();

        _listDataHeader.add(new FilterHeader("Atividade", "Todos os tipos"));
        _listDataHeader.add(new FilterHeader("Dificuldade", "Todos os tipos"));
        _listDataHeader.add(new FilterHeader("Tamanho do percurso (km)", "Todos os tamanhos"));
        _listDataHeader.add(new FilterHeader("Duração do percurso (min.)", "Todas as duraçoes"));

        return _listDataHeader;
    }

    public void cleanCheckboxes() {
        for (Map.Entry<Integer, ArrayList<Integer>> entry : this.childCheckboxState.entrySet()) {
            this.childCheckboxState.put(entry.getKey(), new ArrayList<Integer>());
        }
    }

    public void cleanDistancesAndDurations() {
        this.childDistanceState.clear();
        this.childDurationState.clear();
    }

    public void setHeaderTextToDefault() {
        int groupCount = getGroupCount();

        for (int i = 0; i<groupCount; i++) {
            FilterHeader headerText = (FilterHeader) getGroup(i);
            headerText.setActiveFilter(_listDataHeader.get(i).getActiveFilter());
            notifyDataSetChanged();
        }
        listOfStatusFilters.get(0).clear();
        listOfStatusFilters.get(1).clear();
    }



    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
