package com.example.ai.retrofit;
import com.example.ai.geojson.Geojson;
import com.example.ai.model.PredictedGeojsonInformation;
import com.example.ai.model.Result;
import com.example.ai.model.Route;
import com.mapbox.geojson.FeatureCollection;

import java.util.List;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.PATCH;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;

public interface APICall {

    @GET("api/v1/route/")
    Call<Route> getRouteList(@Query("activity") List<String> activities, @Query("difficulty") List<String> difficulties,
                             @Query("minDistance") String minDistance, @Query("maxDistance") String maxDistance,
                             @Query("minDuration") String minDuration, @Query("maxDuration") String maxDuration);

    /*@GET("api/v1/route/")
    Call<Route> getRouteList();*/

    @GET("api/v1/route/{id}")
    Call<Geojson> getGeojsonById(@Path("id") int id);

    @POST("api/v1/route/")
    Call<PredictedGeojsonInformation> createRouteRequest(@Body RequestBody route);

    @PATCH("api/v1/route/{id}")
    Call<ResponseBody> saveRoute(@Path("id") int id, @Body PredictedGeojsonInformation info);
}
