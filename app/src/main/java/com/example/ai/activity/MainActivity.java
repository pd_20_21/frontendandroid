package com.example.ai.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import android.content.pm.ActivityInfo;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.MenuItem;
import com.example.ai.R;
import com.example.ai.fragment.AskPermissionDialogFragment;
import com.example.ai.fragment.MapFragment;
import com.example.ai.fragment.NewRouteDescriptionFragment;
import com.example.ai.fragment.NewRouteFragment;
import com.example.ai.fragment.RouteFiltersFragment;
import com.example.ai.fragment.RouteList;
import com.example.ai.model.PredictedGeojsonInformation;
import com.google.android.material.bottomnavigation.BottomNavigationView;
import com.mapbox.geojson.FeatureCollection;

public class MainActivity extends AppCompatActivity implements BottomNavigationView.OnNavigationItemSelectedListener, AskPermissionDialogFragment.Communicator,
        NewRouteDescriptionFragment.DeleteFragment, RouteList.ChangeToMapFragment, NewRouteFragment.Communicator {
    private BottomNavigationView navigationView;
    private MapFragment mapFragment = new MapFragment();
    private RouteList routeListFragment = new RouteList();
    private NewRouteFragment newRouteFragment = new NewRouteFragment();
    private FragmentManager fragmentManager = getSupportFragmentManager();
    private Fragment activeFragment = mapFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        getSupportActionBar().hide();
        setContentView(R.layout.activity_main);
        fragmentManager.beginTransaction()
                .add(R.id.fragment_container, routeListFragment, getString(R.string.route_list)).hide(routeListFragment)
                .add(R.id.fragment_container, mapFragment, getString(R.string.map))
                .add(R.id.fragment_container, newRouteFragment, getString(R.string.new_route)).hide(newRouteFragment)
                .commit();
        navigationView = (BottomNavigationView) findViewById(R.id.navigationView);
        navigationView.setOnNavigationItemSelectedListener(this);
        navigationView.getMenu().findItem(R.id.map).setChecked(true);
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        int selectedItem = item.getItemId();

        switch (selectedItem) {
            case R.id.routes_list: {
                if (activeFragment.equals(routeListFragment)) {
                    break;
                }
                if (newRouteFragment.saveFragment != null) {
                    fragmentManager.beginTransaction().hide(activeFragment).hide(newRouteFragment.saveFragment).show(routeListFragment).commit();
                }
                else {
                    fragmentManager.beginTransaction().hide(activeFragment).show(routeListFragment).commit();
                }

                activeFragment = routeListFragment;
                break;
            }
            case R.id.map: {
                if (activeFragment.equals(mapFragment)) { //clicked == R.id.map
                    break;
                }
                if (newRouteFragment.saveFragment != null) {
                    fragmentManager.beginTransaction().hide(activeFragment).hide(newRouteFragment.saveFragment).show(mapFragment).commit();
                }
                else {
                    fragmentManager.beginTransaction().hide(activeFragment).show(mapFragment).commit();
                }

                fragmentManager.beginTransaction().commit();
                activeFragment = mapFragment;
                break;
            }
            case R.id.new_route: {
                if (activeFragment.equals(newRouteFragment)) {
                    break;
                }
                if (newRouteFragment.saveFragment != null) {
                    fragmentManager.beginTransaction().hide(activeFragment).show(newRouteFragment.saveFragment).commit();
                }
                else {
                    fragmentManager.beginTransaction().hide(activeFragment).show(newRouteFragment).commit();
                }
                activeFragment = newRouteFragment;
                break;
            }
        }
        return true;
    }

    @Override
    public void saveTrack(boolean save, PredictedGeojsonInformation dataPredicted) {
        if (save == true) {
            newRouteFragment.saveRoute(dataPredicted);
        }
        else {
            newRouteFragment.cancelRoute();
        }
    }

    @Override
    public void destroy() {
        newRouteFragment.saveFragment = null;
    }

    @Override
    public void changeToMapFragment(FeatureCollection featureCollection) {
        mapFragment.drawLines(featureCollection);
        if (newRouteFragment.saveFragment != null) {
            fragmentManager.beginTransaction().hide(activeFragment).hide(newRouteFragment.saveFragment).show(mapFragment).commit();
        }
        else {
            fragmentManager.beginTransaction().hide(activeFragment).show(mapFragment).commit();
        }
        activeFragment = mapFragment;
    }

    @Override
    public void drawRouteOnMap(FeatureCollection featureCollection) {
        mapFragment.drawLines(featureCollection);
    }
}