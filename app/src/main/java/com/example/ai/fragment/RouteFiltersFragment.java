package com.example.ai.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.example.ai.adapter.ExpandableListViewAdapter;
import com.example.ai.R;
import com.example.ai.activity.MainActivity;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.HashMap;

import butterknife.internal.DebouncingOnClickListener;


public class RouteFiltersFragment extends Fragment {
    View view;
    LinearLayout linearLayout;
    Button applyFiltersButton;
    TextView textViewReset;
    ExpandableListView expandableListViewFilter;

    RouteList parentFragment;
    MainActivity mContext;
    ArrayList<FilterHeader> listDataHeader;
    HashMap<String, ArrayList<String>> listDataChild;

    ExpandableListViewAdapter listAdapter;

    public RouteFiltersFragment(RouteList routeList) {
        parentFragment = routeList;
    }

    /*public static RouteFiltersFragment newInstance() {
        RouteFiltersFragment fragment = new RouteFiltersFragment();
        Bundle args = new Bundle();

        fragment.setArguments(args);
        return fragment;
    }*/

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        expandableListViewFilter = view.findViewById(R.id.expandablelistview_filter);
        linearLayout = view.findViewById(R.id.linearlayout_filter);
        applyFiltersButton = view.findViewById(R.id.applyFiltersBtn);
        prepareListData();
        textViewReset = view.findViewById(R.id.button_reset);

        expandableListViewFilter.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                hideKeyboard(v);
                return false;
            }
        });

        textViewReset.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listAdapter.resetFilter();
            }
        });
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        view = inflater.inflate(R.layout.fragment_filter, container, false);
        return view;
    }

    private void prepareListData() {
        listDataHeader = new ArrayList<FilterHeader>();
        listDataChild = new HashMap<>();
        // Adding child data
        listDataHeader.add(new FilterHeader("Atividade", "Todos os tipos"));
        listDataHeader.add(new FilterHeader("Dificuldade", "Todos os tipos"));
        listDataHeader.add(new FilterHeader("Tamanho do percurso (km)", "Todos os tamanhos"));
        listDataHeader.add(new FilterHeader("Duração do percurso (min.)", "Todas as duraçoes"));




// Adding child data
        ArrayList<String> activity = new ArrayList<>();
        activity.add("Caminhada");
        activity.add("Corrida");
        activity.add("Bicicleta");


        ArrayList<String> difficult = new ArrayList<>();
        difficult.add("Muito Fácil");
        difficult.add("Fácil");
        difficult.add("Média");
        difficult.add("Difícil");
        difficult.add("Muito Difícil");

        ArrayList<String> distance = new ArrayList<>();
        distance.add("Distancia Minima");
        distance.add("Distancia Maxima");

        ArrayList<String> duration = new ArrayList<>();
        duration.add("Duracao Minima");
        duration.add("Duracao Maxima");

        listDataChild.put(listDataHeader.get(0).getTitle(), activity); // Header, Child data
        listDataChild.put(listDataHeader.get(1).getTitle(), difficult);
        listDataChild.put(listDataHeader.get(2).getTitle(), distance);
        listDataChild.put(listDataHeader.get(3).getTitle(), duration);

        listAdapter = new ExpandableListViewAdapter(mContext, listDataHeader, listDataChild, parentFragment);
        expandableListViewFilter.setAdapter(listAdapter);

        applyFiltersButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filtersVerification();       
            }                                
        });                                  
    }


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mContext = (MainActivity) context;
    }


    @Override                                 
    public void onDetach() {
        super.onDetach();
    }

    public void filtersVerification() {
        hideKeyboard(view);

        if (verifyDurationValues() == false) {
            return;
        }

        if (verifyDistanceValues() == false) {
            return;
        }

        parentFragment.addAllChips();
        parentFragment.hideFilterFragment();
        //listAdapter.cleanEditTextView();
        //listAdapter.cleanCheckboxSelection();
        listAdapter.resetFilter();
    }

    public boolean verifyDurationValues() {
        float durMin = 0, durMax = 0;

        if (parentFragment.filter.getMinDuration() != null || parentFragment.filter.getMaxDuration() != null) {
            if (parentFragment.filter.getMinDuration() == null) {
                Toast.makeText(view.getContext(), "Tem que introduzir duração minima!", Toast.LENGTH_SHORT).show();
                return false;
            }

            if (parentFragment.filter.getMaxDuration() == null) {
                Toast.makeText(view.getContext(), "Tem que introduzir duração maxima!", Toast.LENGTH_SHORT).show();
                return false;
            }
            durMin = Float.parseFloat(parentFragment.filter.getMinDuration());
            durMax = Float.parseFloat(parentFragment.filter.getMaxDuration());

            if (durMin >= durMax) {
                Toast.makeText(view.getContext(), "Duracao max tem de ser maior que duracao min", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }

    public boolean verifyDistanceValues() {
        float distMin = 0, distMax = 0;

        if (parentFragment.filter.getMinDistance() != null || parentFragment.filter.getMaxDistance() != null) {
            if (parentFragment.filter.getMinDistance() == null) {
                Toast.makeText(view.getContext(), "Tem que introduzir distancia minima!", Toast.LENGTH_SHORT).show();
                return false;
            }

            if (parentFragment.filter.getMaxDistance() == null) {
                Toast.makeText(view.getContext(), "Tem que introduzir distancia maxima!", Toast.LENGTH_SHORT).show();
                return false;
            }
            distMin = Float.parseFloat(parentFragment.filter.getMinDistance());
            distMax = Float.parseFloat(parentFragment.filter.getMaxDistance());

            if (distMin >= distMax) {
                Toast.makeText(view.getContext(), "Distancia max tem de ser maior que distancia min", Toast.LENGTH_SHORT).show();
                return false;
            }
        }
        return true;
    }  

    public void hideKeyboard(View view) {
        InputMethodManager inputMethodManager =(InputMethodManager) view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
}
