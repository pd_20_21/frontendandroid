package com.example.ai.fragment;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.ai.R;
import com.example.ai.adapter.RecyclerviewItemAdapter;
import com.example.ai.geojson.Geojson;
import com.example.ai.model.Filter;
import com.example.ai.model.Result;
import com.example.ai.model.Route;
import com.example.ai.retrofit.APICall;
import com.example.ai.retrofit.RetrofitClient;
import com.google.android.material.chip.Chip;
import com.google.android.material.chip.ChipGroup;
import com.google.gson.Gson;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.MultiPolygon;

import java.net.InetAddress;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class RouteList extends Fragment {
    View view;
    ChangeToMapFragment changeToMapFragment;
    FragmentTransaction transaction;
    Fragment fragment;
    ChipGroup chipGroup;
    HashMap<String, Chip> chips = new HashMap<>();
    Filter filter = new Filter();

    private RecyclerviewItemAdapter.RecyclerViewClickListener listener;
    private RecyclerView recyclerView;
    private RecyclerviewItemAdapter recyclerviewItemAdapter;
    private List<Result> routeList;

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);

        if (context instanceof RouteList.ChangeToMapFragment) {
            changeToMapFragment = (RouteList.ChangeToMapFragment) getActivity();
        } else {
            throw new ClassCastException(context.toString()
                    + " must implemenet MyListFragment.communicator");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        setOnClickListener();    //PARA O CLICK NA LISTA DE ROTAS
        view = inflater.inflate(R.layout.routes_list_fragment, container, false);
        Button filterButton = (Button) view.findViewById(R.id.filter_button);

        routeList = new ArrayList<>();
        recyclerView = (RecyclerView) view.findViewById(R.id.recycleView);
        LinearLayoutManager layoutManager = new LinearLayoutManager(getContext());
        recyclerviewItemAdapter = new RecyclerviewItemAdapter(routeList, listener);
        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(recyclerviewItemAdapter);

        //linearLayout = (LinearLayout) view.findViewById(R.id.linearlayout_filter);
        chipGroup = view.findViewById(R.id.chipGroup);
        fragment = new RouteFiltersFragment(this);

        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                filter.reset();
                showFilterFragment();
            }
        });
        return view;
    }

    private void setOnClickListener() {
        listener = new RecyclerviewItemAdapter.RecyclerViewClickListener() {
            @Override
            public void onClick(View v, int position) {
                getGeojson(position);
            }
        };
    }

    private void addChip(final String tagName, String filterGroupPosition) {
        Chip chip = new Chip(this.getContext(), null, R.attr.chipStyle);
        chip.setText(tagName);
        chip.setClickable(true);
        chip.setFocusable(true);
        chip.setCloseIconVisible(true);
        chips.put(filterGroupPosition, chip);
        chip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String text = ((Chip) view).getText().toString();
                System.out.println(text);
                //chipGroup.removeView((Chip) view);
                AlphaAnimation animation = new AlphaAnimation(1f, 0f);
                animation.setDuration(250);
                animation.setAnimationListener(new Animation.AnimationListener() {
                    @Override
                    public void onAnimationStart(Animation animation) {

                    }

                    @Override
                    public void onAnimationEnd(Animation animation) {
                        chipGroup.removeView((Chip) view);
                        String groupPosition = getChipGroupPosition(chip);
                        chips.remove(groupPosition);

                        if (groupPosition != null) {
                            Pattern pattern = Pattern.compile("([0-9]+)-([0-9a-zA-Z]+)$");
                            Matcher matcher = pattern.matcher(groupPosition);
                            if (matcher.find()) {
                                int filterId = Integer.parseInt(matcher.group(1));
                                if (filterId < 2 && filterId >= 0) {
                                    String level = matcher.group(2);
                                    if (filterId == 0) {
                                        filter.getActivity().remove(level);
                                        if(filter.getActivity().isEmpty()) {
                                            filter.setActivityToNull();
                                        }
                                    }
                                    else {
                                        filter.getDifficult().remove(level);
                                        if (filter.getDifficult().isEmpty()) {
                                            filter.setDifficultyToNull();
                                        }
                                    }

                                }
                                else {
                                    String minOrMax = matcher.group(2);
                                    Pattern valuePattern = Pattern.compile("^[0-9a-zA-Z\\s\\.]+\\s([0-9]+)$");
                                    Matcher valueMatcher = valuePattern.matcher(chip.getText());
                                    if (valueMatcher.find()) {
                                        if (filterId == 2) {
                                            if (minOrMax.equals("Min")) {
                                                filter.setMinDistance(null);  //valueMatcher.group(1)
                                            }
                                            else {
                                                filter.setMaxDistance(null);  //valueMatcher.group(1)
                                            }
                                        }
                                        else {
                                            if (filterId == 3) {
                                                if (minOrMax.equals("Min")) {
                                                    filter.setMinDuration(null);  //valueMatcher.group(1)
                                                } else {
                                                    filter.setMaxDuration(null);  //valueMatcher.group(1)
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                            routeListRequest();
                        }
                    }

                    @Override
                    public void onAnimationRepeat(Animation animation) {

                    }
                });
                view.startAnimation(animation);
            }
        });
        chipGroup.addView(chip);
    }

    public String getChipGroupPosition(Chip chip) {
        for (Entry<String, Chip> chipAux : chips.entrySet()) {
            if (chipAux.getValue().equals(chip)) {
                return chipAux.getKey();
            }
        }
        return null;
    }

    public void addAllChips() {
        chipGroup.removeAllViews();
        if (filter.getActivity() != null && !filter.getActivity().isEmpty()) {
            addChipFromArray(filter.getActivity(), 0, "Actividade: ");
        }
        else {
            filter.setActivityToNull();
        }

        if (filter.getDifficult() != null && !filter.getDifficult().isEmpty()) {
            addChipFromArray(filter.getDifficult(), 1, "Dificuldade: ");
        }
        else {
            filter.setDifficultyToNull();
        }

        if (filter.getMinDistance() != null && !filter.getMinDistance().isEmpty()) {
            addChip("Dist. Min " + filter.getMinDistance(), 2 + "-Min");
        }
        else {
            filter.setMinDistance(null);
        }

        if (filter.getMaxDistance() != null && !filter.getMaxDistance().isEmpty()) {
            addChip("Dist. Max " + filter.getMaxDistance(), 2 + "-Max");
        }
        else {
            filter.setMaxDistance(null);
        }

        if (filter.getMinDuration() != null && !filter.getMinDuration().isEmpty()) {
            addChip("Dur. Min " + filter.getMinDuration(), 3 + "-Min");
        }
        else {
            filter.setMinDuration(null);
        }

        if (filter.getMaxDuration() != null && !filter.getMaxDuration().isEmpty()) {
            addChip("Dur. Max " + filter.getMaxDuration(), 3 + "-Max");
        }
        else {
            filter.setMaxDuration(null);
        }
        routeListRequest();
    }

    public void routeListRequest() {
        APICall apiCall = RetrofitClient.getClient().create(APICall.class);

        if (isNetworkConnected()) {
            Call<Route> call = apiCall.getRouteList(filter.getActivity(), filter.getDifficult(), filter.getMinDistance(), filter.getMaxDistance(),
                    filter.getMinDuration(), filter.getMaxDuration());
            call.enqueue(new Callback<Route>() {
                @Override
                public void onResponse(Call<Route> call, Response<Route> response) {
                    Route route = response.body();
                    routeList = route.getResults();
                    recyclerviewItemAdapter.setRouteList(routeList);
                    recyclerviewItemAdapter.notifyDataSetChanged();
                    Toast.makeText(view.getContext(), "Lista Rotas com sucesso!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<Route> call, Throwable t) {
                    Toast.makeText(view.getContext(), "Não foram encontradas rotas!", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void getGeojson(int position) {
        APICall apiCall = RetrofitClient.getClient().create(APICall.class);
        Call<Geojson> call = apiCall.getGeojsonById(routeList.get(position).getId());

        if (isNetworkConnected()) {
            call.enqueue(new Callback<Geojson>() {
                @Override
                public void onResponse(Call<Geojson> call, Response<Geojson> response) {
                    Gson gson = new Gson();
                    String json = gson.toJson(response.body().result);
                    FeatureCollection featureCollection =FeatureCollection.fromJson(json);
                    System.out.println(featureCollection.features().size());
                    changeToMapFragment.changeToMapFragment(featureCollection);
                    Toast.makeText(view.getContext(), "GeoJson recebido com sucesso!!!", Toast.LENGTH_SHORT).show();
                }

                @Override
                public void onFailure(Call<Geojson> call, Throwable t) {
                    Toast.makeText(view.getContext(), "Erro em obter Geojson", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void addChipFromArray(ArrayList<String> list, int groupPosition, String filterType) {
        for (int i = 0 ; i<list.size(); i++) {
            addChip(filterType + list.get(i), groupPosition + "-" + list.get(i));
        }
    }

    public void addFilter(String filterType, String value) {
        switch (filterType) {
            case "Atividade":
                filter.setActivity(getActivityLevel(value));
                break;
            case "Dificuldade":
                filter.setDifficult(getDifficultyLevel(value));
                break;
            case "Distancia Minima":
                filter.setMinDistance(value);
                break;
            case "Distancia Maxima":
                filter.setMaxDistance(value);
                break;
            case "Duracao Minima":
                filter.setMinDuration(value);
                break;
            case "Duracao Maxima":
                filter.setMaxDuration(value);
                break;
            default:
                break;
        }
    }

    public String getActivityLevel(String value) {
        switch (value) {
            case "Caminhada":
                return String.valueOf(1);
            case "Corrida":
                return String.valueOf(2);
            case "Bicicleta":
                return String.valueOf(3);
            default:
                return String.valueOf(-1);
        }
    }

    public String getDifficultyLevel(String value) {
        switch (value) {
            case "Muito Fácil":
                return String.valueOf(1);
            case "Fácil":
                return String.valueOf(2);
            case "Média":
                return String.valueOf(3);
            case "Difícil":
                return String.valueOf(4);
            case "Muito Difícil":
                return String.valueOf(5);
            default:
                return String.valueOf(-1);
        }
    }

    public void removeFilter(String filterType, String value) {
        switch (filterType) {
            case "Atividade":
                if (filter.getActivity() != null) {
                    filter.getActivity().remove(getActivityLevel(value));
                }
                break;
            case "Dificuldade":
                if (filter.getDifficult() != null) {
                    filter.getDifficult().remove(getDifficultyLevel(value));
                }
                break;
            default:
                break;
        }
    }


    public void hideFilterFragment() {
        transaction = getActivity().getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frame, fragment, "filter").hide(fragment).commit();
    }


    private void showFilterFragment() {
        transaction = getActivity().getSupportFragmentManager().beginTransaction();
        if (fragment.isAdded()) {
            transaction.show(fragment);
        }
        else {
            transaction.add(R.id.frame, fragment, "filter");
        }
        transaction.commit();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public interface ChangeToMapFragment {
        void changeToMapFragment(FeatureCollection featureCollection);
    }
}
