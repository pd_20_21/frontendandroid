package com.example.ai.fragment;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;

import com.example.ai.R;
import com.example.ai.geojson.Geojson;
import com.example.ai.model.PredictedGeojsonInformation;
import com.example.ai.retrofit.APICall;
import com.example.ai.retrofit.RetrofitClient;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.MultiPoint;

import java.io.IOException;
import java.net.InetAddress;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class AskPermissionDialogFragment extends DialogFragment implements View.OnClickListener {
    final MediaType CONTENT_TYPE = MediaType.parse("application/json");
    View view;
    Button yesButton, noButton;
    Communicator communicator;
    FeatureCollection featureCollection;

    public AskPermissionDialogFragment(FeatureCollection featureCollection) {
        this.featureCollection = featureCollection;
    }

    @Override
    public void onAttach(Context context) {

        super.onAttach(context);

        if (context instanceof Communicator) {
            communicator = (Communicator) getActivity();
        } else {
            throw new ClassCastException(context.toString()
                    + " must implemenet MyListFragment.communicator");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        setCancelable(false);
        getDialog().setTitle("Title");

        view = inflater.inflate(R.layout.ask_permission_dialog, null, false);

        yesButton = (Button) view.findViewById(R.id.yesbtn);
        noButton = (Button) view.findViewById(R.id.nobtn);

        // setting onclick listener for buttons
        yesButton.setOnClickListener(this);
        noButton.setOnClickListener(this);

        return view;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.yesbtn:
                //createGeoJson();
                postRoute();
                break;
            case R.id.nobtn:
                dismiss();
                communicator.saveTrack(false, null);
                break;
        }
    }

    /*public void createGeoJson(){
        MultiPoint multiPoint = MultiPoint.fromLngLats(newRouteFragment.coordinates);
        LineString lineString = LineString.fromLngLats(multiPoint);
        //MultiLineString multiLineString = MultiLineString.fromLineString(lineString);

        newRouteFragment.feature = Feature.fromGeometry(lineString);

        //FeatureCollection featureCollection = FeatureCollection.fromFeature(feature);

        JsonElement coordTimesJson = new Gson().toJsonTree(newRouteFragment.coordTimes);
        System.out.println(coordTimesJson);
        newRouteFragment.feature.addProperty("coordTimes", coordTimesJson);
        newRouteFragment.featureCollection = FeatureCollection.fromFeature(newRouteFragment.feature);
        System.out.println(newRouteFragment.featureCollection.toJson());

        /*Gson gson = new Gson();
        System.out.println(gson.toJson(newRouteFragment.featureCollection));
    }*/

    public void postRoute() {
        APICall apiCall = RetrofitClient.getClient().create(APICall.class);
        RequestBody requestBody = RequestBody.create(CONTENT_TYPE, featureCollection.toJson());
        Call<PredictedGeojsonInformation> call = apiCall.createRouteRequest(requestBody);

        if (isNetworkConnected()) {
            call.enqueue(new Callback<PredictedGeojsonInformation>() {
                @Override
                public void onResponse(Call<PredictedGeojsonInformation> call, Response<PredictedGeojsonInformation> response) {
                    if (response.code() == 200) {
                        PredictedGeojsonInformation info = response.body();

                        Toast.makeText(view.getContext(), "Informação recebida com sucesso!!!", Toast.LENGTH_SHORT).show();
                        dismiss();
                        communicator.saveTrack(true, info);
                    }
                }

                @Override
                public void onFailure(Call<PredictedGeojsonInformation> call, Throwable t) {
                    Toast.makeText(view.getContext(), "Erro em obter informação do percurso", Toast.LENGTH_SHORT).show();
                    dismiss();
                    communicator.saveTrack(false, null);
                }
            });
        }
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }

    public interface Communicator {
        public void saveTrack(boolean save, PredictedGeojsonInformation dataPredicted);
    }
}
