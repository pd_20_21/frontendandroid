package com.example.ai.fragment;

import android.app.Activity;
import android.content.Context;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import com.example.ai.R;
import com.example.ai.model.PredictedGeojsonInformation;
import com.example.ai.retrofit.APICall;
import com.example.ai.retrofit.RetrofitClient;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;

import java.io.IOException;
import java.net.InetAddress;

import okhttp3.MediaType;
import okhttp3.RequestBody;
import okhttp3.ResponseBody;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class NewRouteDescriptionFragment extends Fragment {
    View view;
    DeleteFragment deleteFragment;
    PredictedGeojsonInformation dataPredicted;
    Spinner difficultySpinner;
    Spinner activitySpinner;
    EditText editText;

    public NewRouteDescriptionFragment(PredictedGeojsonInformation dataPredicted) {
        this.dataPredicted = dataPredicted;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.new_route_final_fragment, container, false);
        editText = (EditText) view.findViewById(R.id.route_name);
        difficultySpinner = (Spinner) view.findViewById(R.id.spinnerDifficultType);
        activitySpinner = (Spinner) view.findViewById(R.id.spinnerActivityType);
        ArrayAdapter<CharSequence> difficultyAdapter = ArrayAdapter.createFromResource(view.getContext(),
                R.array.difficult_array, android.R.layout.simple_spinner_item);
        ArrayAdapter<CharSequence> activityAdapter = ArrayAdapter.createFromResource(view.getContext(),
                R.array.activity_array, android.R.layout.simple_spinner_item);
        difficultyAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        activityAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        difficultySpinner.setAdapter(difficultyAdapter);
        activitySpinner.setAdapter(activityAdapter);
        ImageView backArrow = view.findViewById(R.id.back_arrow);
        editText.setText(dataPredicted.getName());
        difficultySpinner.setSelection(Integer.parseInt(dataPredicted.getDifficulty()) -1);
        activitySpinner.setSelection(Integer.parseInt(dataPredicted.getActivityType())-1);
        backArrow.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                destroyFragment();
            }
        });

        Button submitRoute = (Button) view.findViewById(R.id.submit_route);
        submitRoute.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                saveRoute();
            }
        });

        LinearLayout linearLayout = (LinearLayout) view.findViewById(R.id.new_route_final_id);
        linearLayout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                hideSoftKeyboard();
                return false;
            }
        });
        return view;
    }

    public void saveRoute() {
        APICall apiCall = RetrofitClient.getClient().create(APICall.class);
        Call<ResponseBody> call = apiCall.saveRoute(dataPredicted.getId(), dataPredicted);
        updateRouteDescription();  //update the route description

        if (isNetworkConnected()) {
            call.enqueue(new Callback<ResponseBody>() {
                @Override
                public void onResponse(Call<ResponseBody> call, Response<ResponseBody> response) {
                    if (response.code() == 200) {
                        destroyFragment();
                        Toast.makeText(view.getContext(), "Rota gravada com sucesso!!!", Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onFailure(Call<ResponseBody> call, Throwable t) {
                    destroyFragment();
                    Toast.makeText(view.getContext(), "Erro em gravar!", Toast.LENGTH_SHORT).show();
                }
            });
        }
    }

    public void updateRouteDescription() {
        dataPredicted.setDifficulty(Integer.toString(difficultySpinner.getSelectedItemPosition()) +1);
        dataPredicted.setActivityType(Integer.toString(activitySpinner.getSelectedItemPosition() +1));
        dataPredicted.setName(editText.getText().toString());
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        deleteFragment = (NewRouteDescriptionFragment.DeleteFragment) getActivity();
    }

    public void destroyFragment() {
        FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
        Fragment newRouteFragment = fragmentManager.findFragmentByTag("New Route");
        fragmentManager.beginTransaction()
                .remove(this)
                .show(newRouteFragment)
                .commit();
        deleteFragment.destroy();
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    public void hideSoftKeyboard() {
        InputMethodManager imm = (InputMethodManager)
                view.getContext().getSystemService(Activity.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public interface DeleteFragment {
        void destroy();
    }

    private boolean isNetworkConnected() {
        ConnectivityManager cm = (ConnectivityManager) getContext().getSystemService(Context.CONNECTIVITY_SERVICE);

        return cm.getActiveNetworkInfo() != null && cm.getActiveNetworkInfo().isConnected();
    }
}