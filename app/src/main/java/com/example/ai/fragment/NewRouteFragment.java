package com.example.ai.fragment;

import android.content.Context;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.SystemClock;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Chronometer;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import com.example.ai.R;
import com.example.ai.model.PredictedGeojsonInformation;
import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.mapbox.geojson.Feature;
import com.mapbox.geojson.FeatureCollection;
import com.mapbox.geojson.LineString;
import com.mapbox.geojson.MultiPoint;
import com.mapbox.geojson.Point;

import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.ArrayList;

public class NewRouteFragment extends Fragment {
    Communicator communicator;
    private static final SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss'Z'");
    View view;
    Button startChronometer;
    Button pauseChronometer;
    Button stopChronometer;
    private Chronometer chronometer;
    private long pauseOffSet;
    private boolean running = false;
    private long nTicks = 0;
    private long lastTicker = 0;
    public Fragment saveFragment;
    private LocationManager locationManager;
    public Feature feature;
    public FeatureCollection featureCollection;
    public ArrayList<Point> coordinates = new ArrayList<>();
    public ArrayList<String> coordTimes = new ArrayList<>();
    Handler handler = new Handler();
    Runnable runnable;


    @Override
    public void onAttach(Context context) {

        super.onAttach(context);

        if (context instanceof RouteList.ChangeToMapFragment) {
            communicator = (NewRouteFragment.Communicator) getActivity();
        } else {
            throw new ClassCastException(context.toString()
                    + " must implemenet MyListFragment.communicator");
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.new_route_fragment, container, false);
        startChronometer = (Button) view.findViewById(R.id.start);
        pauseChronometer = (Button) view.findViewById(R.id.pause);
        stopChronometer = (Button) view.findViewById(R.id.stop);
        locationManager = (LocationManager) view.getContext().getSystemService(Context.LOCATION_SERVICE);

        startChronometer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startChronometer();
                startChronometer.setEnabled(false);
                pauseChronometer.setEnabled(true);
                stopChronometer.setEnabled(true);
            }
        });

        pauseChronometer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacksAndMessages(null);
                pauseChronometer();
                startChronometer.setText("Continuar");
                startChronometer.setEnabled(true);
                pauseChronometer.setEnabled(false);
            }
        });

        stopChronometer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                handler.removeCallbacksAndMessages(null);
                FragmentManager manager = getFragmentManager();
                stopChronometer();
                createGeoJson();
                AskPermissionDialogFragment newDialog = new AskPermissionDialogFragment(featureCollection);
                newDialog.show(manager, "permissionDialog");
            }
        });

        chronometer = view.findViewById(R.id.chronometer);
        chronometer.setBase(SystemClock.elapsedRealtime());

        chronometer.setOnChronometerTickListener(new Chronometer.OnChronometerTickListener() {
            @Override
            public void onChronometerTick(Chronometer chronometer) {
                runnable = new Runnable() {
                    @Override
                    public void run() {
                        getCoordsEverySec(SystemClock.elapsedRealtime() - chronometer.getBase() - lastTicker);
                        handler.postDelayed(this, 1000);
                    }
                };
                handler.post(runnable);
            }
        });

        return view;
    }

    public void getCoordsEverySec(long nTicks) {
        if (nTicks >= 1000 && running) {
            lastTicker =  SystemClock.elapsedRealtime() - chronometer.getBase();
            try {
                double latitude = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude();
                double longitude = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude();
                double altitude = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getAltitude();
                coordinates.add(Point.fromLngLat(longitude, latitude, altitude));

                Timestamp timestamp = new Timestamp(System.currentTimeMillis());
                coordTimes.add(sdf.format(timestamp));
                createGeoJson();
                communicator.drawRouteOnMap(featureCollection);
            } catch ( SecurityException e ) {
                e.printStackTrace(); }
        }
    }

    public void createGeoJson(){
        MultiPoint multiPoint = MultiPoint.fromLngLats(coordinates);
        LineString lineString = LineString.fromLngLats(multiPoint);
        //MultiLineString multiLineString = MultiLineString.fromLineString(lineString);

        feature = Feature.fromGeometry(lineString);

        //FeatureCollection featureCollection = FeatureCollection.fromFeature(feature);

        JsonElement coordTimesJson = new Gson().toJsonTree(coordTimes);
        //System.out.println(coordTimesJson);
        feature.addProperty("coordTimes", coordTimesJson);
        featureCollection = FeatureCollection.fromFeature(feature);
        System.out.println(featureCollection.toJson());

    }


    public void startChronometer() {
        if (!running) {
            chronometer.setBase(SystemClock.elapsedRealtime() - pauseOffSet);
            chronometer.start();
            running = true;
        }
    }

    public void pauseChronometer() {
        if (running) {
            chronometer.stop();
            pauseOffSet = SystemClock.elapsedRealtime() - chronometer.getBase();
            running = false;
        }
    }

    public void stopChronometer() {
        pauseOffSet = 0;
        running = false;
        chronometer.stop();
    }

    public void saveRoute(PredictedGeojsonInformation dataPredicted) {
        FragmentTransaction transaction = getActivity().getSupportFragmentManager().beginTransaction();

        saveFragment = new NewRouteDescriptionFragment(dataPredicted);
        transaction.add(R.id.save_route, saveFragment, "saveFragment");
        transaction.show(saveFragment);
        transaction.hide(this);
        transaction.commit();
        cancelRoute();
    }

    public void cancelRoute() {
        startChronometer.setText("Começar!");
        startChronometer.setEnabled(true);
        pauseChronometer.setEnabled(false);
        stopChronometer.setEnabled(false);
        featureCollection = null;
        chronometer.stop();
        handler.removeCallbacksAndMessages(null);
        chronometer.setBase(SystemClock.elapsedRealtime());
        coordinates.clear();
        coordTimes.clear();
        lastTicker = 0;
    }

    public interface Communicator {
        void drawRouteOnMap(FeatureCollection featureCollection);
    }
}