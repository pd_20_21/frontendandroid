package com.example.ai.geojson;

import com.google.gson.annotations.SerializedName;

public class Feature {
    @SerializedName("type")
    public String type;

    @SerializedName("geometry")
    public Geometry geometry;

    @SerializedName("properties")
    public Properties properties;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Geometry getGeometry() {
        return geometry;
    }

    public void setGeometry(Geometry geometry) {
        this.geometry = geometry;
    }

    public Properties getProperties() {
        return properties;
    }

    public void setProperties(Properties properties) {
        this.properties = properties;
    }
}
