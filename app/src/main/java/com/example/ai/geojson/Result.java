package com.example.ai.geojson;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Result {
    @SerializedName("type")
    public String type;

    @SerializedName("features")
    public List<Feature> features;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Feature> getFeatures() {
        return features;
    }

    public void setFeatures(List<Feature> features) {
        this.features = features;
    }
}
