package com.example.ai.geojson;

import com.google.gson.annotations.SerializedName;

import java.util.Date;
import java.util.List;

public class Properties {
    @SerializedName("name")
    public String name;

    @SerializedName("time")
    public Date time;

    @SerializedName("desc")
    public String type;

    @SerializedName("coordTimes")
    public List<Date> coordTimes;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public List<Date> getCoordTimes() {
        return coordTimes;
    }

    public void setCoordTimes(List<Date> coordTimes) {
        this.coordTimes = coordTimes;
    }
}
