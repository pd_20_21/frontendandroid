package com.example.ai.model;
import com.google.gson.annotations.SerializedName;

public class Result {
    @SerializedName("id")
    int id;

    @SerializedName("name")
    String routeName;

    @SerializedName("activitytype")
    int activityId;

    @SerializedName("difficulty")
    int difficulty;

    @SerializedName("distance")
    String distance;

    /*public Route(String routeName, String distance) {
        this.routeName = routeName;
        this.distance = distance;
    }*/

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRouteName() {
        return routeName;
    }

    public void setRouteName(String routeName) {
        this.routeName = routeName;
    }

    public int getActivityId() {
        return activityId;
    }

    public void setActivityId(int activityId) {
        this.activityId = activityId;
    }

    public int getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(int difficulty) {
        this.difficulty = difficulty;
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = distance;
    }
}
