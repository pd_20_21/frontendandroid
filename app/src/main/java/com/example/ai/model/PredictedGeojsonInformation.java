package com.example.ai.model;

import com.google.gson.annotations.SerializedName;

public class PredictedGeojsonInformation {
    @SerializedName("id")
    int id;

    @SerializedName("name")
    String name;

    @SerializedName("activityType")
    String activityType;

    @SerializedName("difficulty")
    String difficulty;

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getActivityType() {
        return activityType;
    }

    public void setActivityType(String activityType) {
        this.activityType = activityType;
    }

    public String getDifficulty() {
        return difficulty;
    }

    public void setDifficulty(String difficulty) {
        this.difficulty = difficulty;
    }
}
