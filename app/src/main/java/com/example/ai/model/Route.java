package com.example.ai.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class Route {
    @SerializedName("count")
    public int count;

    @SerializedName("totalCount")
    public int totalCount;

    @SerializedName("results")
    public List<Result> results;

    public int getCount() {
        return count;
    }

    public void setCount(int count) {
        this.count = count;
    }

    public int getTotalCount() {
        return totalCount;
    }

    public void setTotalCount(int totalCount) {
        this.totalCount = totalCount;
    }

    public List<Result> getResults() {
        return results;
    }

    public void setResults(List<Result> results) {
        this.results = results;
    }
}
