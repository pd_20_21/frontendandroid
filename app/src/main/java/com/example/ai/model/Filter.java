package com.example.ai.model;

import java.util.ArrayList;

public class Filter {
    ArrayList<String> activity;

    ArrayList<String> difficulty;

    String minDistance;

    String maxDistance;

    String minDuration;

    String maxDuration;

    public ArrayList<String> getActivity() {
        return activity;
    }

    public void setActivity(String activityValue) {
        if (this.getActivity() == null) {
            this.activity = new ArrayList<>();
        }
        this.activity.add(activityValue);
    }

    public void setActivityToNull() {
        if (this.activity != null) {
            this.activity.clear();
            this.activity = null;
        }
    }

    public ArrayList<String> getDifficult() {
        return difficulty;
    }

    public void setDifficult(String difficultValue) {
        if (this.getDifficult() == null) {
            this.difficulty = new ArrayList<>();
        }
        this.difficulty.add(difficultValue);
    }

    public void setDifficultyToNull() {
        if (this.difficulty != null) {
            this.difficulty.clear();
            this.difficulty = null;
        }
    }

    public String getMinDistance() {
        return minDistance;
    }

    public void setMinDistance(String minDistance) {
        if (minDistance == "") {
            this.minDistance = null;
            return;
        }
        if (this.minDistance == null) {
            this.minDistance = new String();
        }
        this.minDistance = minDistance;
    }

    public String getMaxDistance() {
        return maxDistance;
    }

    public void setMaxDistance(String maxDistance) {
        if (maxDistance == "") {
            this.maxDistance = null;
            return;
        }
        if (this.maxDistance == null) {
            this.maxDistance = new String();
        }
        this.maxDistance = maxDistance;
    }

    public String getMinDuration() {
        return minDuration;
    }

    public void setMinDuration(String minDuration) {
        if (minDuration == "") {
            this.minDuration = null;
            return;
        }
        if (this.minDuration == null) {
            this.minDuration = new String();
        }
        this.minDuration = minDuration;
    }

    public String getMaxDuration() {
        return maxDuration;
    }

    public void setMaxDuration(String maxDuration) {
        if (maxDuration == "") {
            this.maxDuration = null;
            return;
        }
        if (this.maxDuration == null) {
            this.maxDuration = new String();
        }
        this.maxDuration = maxDuration;
    }

    public void reset() {
        this.setDifficultyToNull();
        this.setActivityToNull();
        this.minDuration = null;
        this.maxDuration = null;
        this.minDistance = null;
        this.maxDistance = null;
    }
}
